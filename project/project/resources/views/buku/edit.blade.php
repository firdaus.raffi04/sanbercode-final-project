@extends('layouts.master')
@section('title')
   Halaman Edit
@endsection
@section('subtitle')
   Edit
@endsection
@section('content')
<form action="/buku/{{$buku->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" value="{{$buku->judul}}" placeholder="Masukkan judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Pengarang</label>
        <input type="text" class="form-control" name="pengarang" value="{{$buku->pengarang}}" id="pengarang" placeholder="Masukkan pengarang">
        @error('pengarang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Penerbit</label>
        <input type="text" class="form-control" name="penerbit" value="{{$buku->penerbit}}" id="penerbit" placeholder="Masukkan penerbit">
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun"  value="{{$buku->tahun}}" id="tahun" placeholder="Masukkan tahun">
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" class="form-control" name="gambar"  value="{{$buku->gambar}}" id="gambar">
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
