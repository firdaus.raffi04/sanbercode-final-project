<!DOCTYPE html>
<html lang="en-US" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  
    Document Title
    =============================================
    -->
    <title>@yield('title')</title>
    <!--  
    Favicons
    =============================================
    -->
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('titanm/assets/images/favicons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <!--  
    Stylesheets
    =============================================
    
    -->
    <!-- Default stylesheets-->
    <link href="{{asset('titanm/assets/lib/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/animate.css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/components-font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/et-line-font/et-line-font.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/flexslider/flexslider.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/owl.carousel/dist/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/simple-text-rotator/simpletextrotator.css')}}" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="{{asset('/titanm/assets/css/style.css')}}" rel="stylesheet">
    <link id="color-scheme" href="{{asset('titanm/assets/css/colors/default.css')}}" rel="stylesheet">
  </head>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <div class="page-loader">
        <div class="loader">Loading...</div>
      </div>
      @include('element.navcust')
      <div class="main">
        <section class="module bg-dark-60 about-page-header" data-background="{{asset('BG1.jpg')}}">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">@yield('judul')</h2>
                <div class="module-subtitle font-serif">@yield('subtitle')</div>
              </div>
            </div>
          </div>
        </section>
        <section class="module">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                @yield('content')
                {{-- <h5 class="font-alt">We’re a digital creative agency</h5><br/>
                <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words.</p>
                <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary.</p>
              </div>
              <div class="col-sm-6">
                <h6 class="font-alt"><span class="icon-tools-2"></span> Development
                </h6>
                <div class="progress">
                  <div class="progress-bar pb-dark" aria-valuenow="60" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="font-alt"></span></div>
                </div>
                <h6 class="font-alt"><span class="icon-strategy"></span> Branding
                </h6>
                <div class="progress">
                  <div class="progress-bar pb-dark" aria-valuenow="80" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="font-alt"></span></div>
                </div>
                <h6 class="font-alt"><span class="icon-target"></span> Marketing
                </h6>
                <div class="progress">
                  <div class="progress-bar pb-dark" aria-valuenow="50" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="font-alt"></span></div>
                </div>
                <h6 class="font-alt"><span class="icon-camera"></span> Photography
                </h6>
                <div class="progress">
                  <div class="progress-bar pb-dark" aria-valuenow="90" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="font-alt"></span></div>
                </div> --}}
              </div>
            </div>
          </div>
        </section>
        @include('element.credit')
        <hr class="divider-d">
        <footer class="footer bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2017&nbsp;<a href="index.html">TitaN</a>, All Rights Reserved</p>
              </div>
              <div class="col-sm-6">
                <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <!--  
    JavaScripts
    =============================================
    -->
    <script src="{{asset('titanm/assets/lib/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/wow/dist/wow.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/isotope/dist/isotope.pkgd.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/imagesloaded/imagesloaded.pkgd.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/flexslider/jquery.flexslider.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/owl.carousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/smoothscroll.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/magnific-popup/dist/jquery.magnific-popup.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js')}}"></script>
    <script src="{{asset('titanm/assets/js/plugins.js')}}"></script>
    <script src="{{asset('titanm/assets/js/main.js')}}"></script>
  </body>
</html>