<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="/home">Perpustakaan</a>
    </div>
    <div class="collapse navbar-collapse" id="custom-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown"><a href="/home">Home</a></li>
        <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Category</a>
          <ul class="dropdown-menu">
            <li class="dropdown"><a href="#">Novel</a></li>
            <li class="dropdown"><a href="#">Biografi</a></li>
            <li class="dropdown"><a href="#">Komik</a></li>
            <li class="dropdown"><a href="#">Kamus</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="" href="/book">Book</a></li>
        <li class="dropdown"><a class="" href="/cust">Member</a></li>
        <li class="">
          <a href="/login" class="btn btn-danger btn-sm-auto">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>