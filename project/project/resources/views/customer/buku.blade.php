@extends('layouts.book')
@section('title')
    Buku
@endsection
@section('judul')
    BUKU KAMI
@endsection
@section('subtitle')
    BERIKUT MERUPAKAN BUKU-BUKU YANG TERDAPAT DI WEB KAMI,
    ANDA DAPAT MEMINJAM BUKU-BUKU YANG TERSEDIA.
@endsection

@section('content')
<section class="module-small">
  <div class="container">
    <div class="row multi-columns-row">
      @forelse ($buku as $key=>$value)
        <div class="col-sm-6 col-md-3 col-lg-3">
          <div class="shop-item">
            <div class="shop-item-image"><img src="{{$value->gambar}}" alt="Accessories Pack"/>
              <div class="shop-item-detail">
                <h6 class="font-alt" style="color: black">{{$value->judul}} ({{$value->tahun}})<br>Pengarang: {{$value->pengarang}}<br>Penerbit: {{$value->penerbit}}</h6>
                <a class="btn btn-round btn-b" href="/pinjam/{{$value->id}}"><span>Pinjam Buku</span></a>
              </div>
            </div>
            <h4 class="shop-item-title font-alt"><a href="/pinjam/{{$value->id}}">{{$value->judul}}</a></h4>
          </div>
        </div>
      @empty
          <tr colspan="3">
            <td>No data</td>
          </tr>  
      @endforelse  
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="pagination font-alt"><a href="#"><i class="fa fa-angle-left"></i></a><a class="active" href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#"><i class="fa fa-angle-right"></i></a></div>
      </div>
    </div>
  </div>
</section>
@endsection