@extends('layouts.master')
@section('title')
   Halaman Tampil
@endsection
@section('subtitle')
   Tampil
@endsection
@section('content')
<a href="/buku/create" class="btn btn-primary btn-sm">Tambah Buku</a>
<table class="table table-bordered mt-2" id="#example1">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Judul</th>
        <th scope="col">Pengarang</th>
        <th scope="col">Penerbit</th>
        <th scope="col">Tahun</th>
        <th scope="col">Gambar</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->pengarang}}</td>
                    <td>{{$value->penerbit}}</td>
                    <td>{{$value->tahun}}</td>
                    <td>{{$value->gambar}}</td>
                    <td>
                        <form action="/buku/{{$value->id}}" method="post">
                            @method('delete')
                            @csrf
                        <a href="/buku/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/buku/{{$value->id}}/edit" onclick="return confirm('Yakin mau edit?')" class="btn btn-success btn-sm">Edit</a>
                        <input type="submit" value="Hapus" onclick="return confirm('Yakin mau hapus?')" class="btn btn-danger btn-sm">
                    </form>
                    </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse
    </tbody>
</table>
@endsection