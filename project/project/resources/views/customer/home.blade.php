<!DOCTYPE html>
<html lang="en-US" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  
    Document Title
    =============================================
    -->
    <title>Home</title>
    <!--  
    Favicons
    =============================================
    -->
    <!-- Default stylesheets-->
    <link href="{{asset('titanm/assets/lib/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/animate.css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/components-font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/et-line-font/et-line-font.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/flexslider/flexslider.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/owl.carousel/dist/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{asset('/titanm/assets/lib/simple-text-rotator/simpletextrotator.css')}}" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="{{asset('/titanm/assets/css/style.css')}}" rel="stylesheet">
    <link id="color-scheme" href="{{asset('titanm/assets/css/colors/default.css')}}" rel="stylesheet">
  </head>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>
        @include('element.navcust')
      <section class="home-section bg-dark-30" id="home" data-background="{{asset('BGHOME.jpg')}}">
        
        <div class="titan-caption">
          <div class="caption-content">
            <div class="font-alt mb-30 titan-title-size-1">Halo &amp; Selamat Datang</div>
            <div class="font-alt mb-40 titan-title-size-4">Perpustakaan</div><a class="section-scroll btn btn-border-w btn-round" href="#about">Learn More</a>
          </div>
        </div>
      </section>
      <div class="main">
        <section class="module" id="about">
          <div class="container">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <h2 class="module-title font-alt">Selamat datang di Perpustakaan</h2>
                <div class="module-subtitle font-serif large-text">Misi kami adalah meningkatkan literasi dan memberikan kemudahan akses pada dunia pengetahuan di seluruh Indonesia dengan memanfaatkan teknologi.</div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2 col-sm-offset-5">
                <div class="large-text align-center"><a class="section-scroll" href="#pinjam"><i class="fa fa-angle-down"></i></a></div>
              </div>
            </div>
          </div>
        </section>
        <hr class="divider-w">
        <section class="module-small bg-dark" id="pinjam">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-8 col-lg-6 col-lg-offset-2">
                <div class="callout-text font-alt">
                  <h3 class="callout-title">INGIN MEMINJAM BUKU?</h3>
                  <p>KLIK UNTUK MEMINJAM BUKU</p>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-2">
                <div class="callout-btn-box"><a class="btn btn-w btn-round" href="/book">Pinjam Buku</a></div>
              </div>
            </div>
          </div>
        </section>
        <hr class="divider-w">
        <section class="module" id="team">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Meet Our Team</h2>
                <div class="module-subtitle font-serif">Web ini dibuat oleh 2 orang.</div>
              </div>
            </div>
            <div class="row">
                <div class="d-flex justify-content-center">
              <div class="mb-sm-20 wow fadeInUp col-sm-12 col-md-6" onclick="wow fadeInUp">
                <div class="team-item">
                  <div class="team-image"><img src="{{asset('titanm/assets/images/team-3.jpg')}}" alt="Member Photo"/>
                    <div class="team-detail">
                      <h5 class="font-alt">Hello</h5>
                      <p class="font-serif">Saya seorang pelajar dari SMK TELKOM JAKARTA dan saya berprofesi sebagai frontend developer</p>
                      <div class="team-social"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a></div>
                    </div>
                  </div>
                  <div class="team-descr font-alt">
                    <div class="team-name">Raffi Akbar Firdaus</div>
                    <div class="team-role">Pelajar SMK TELKOM JAKARTA</div>
                  </div>
                </div>
              </div>
              <div class="mb-sm-20 wow fadeInUp col-sm-12 col-md-6" onclick="wow fadeInUp">
                <div class="team-item">
                  <div class="team-image"><img src="{{asset('titanm/assets/images/team-4.jpg')}}" alt="Member Photo"/>
                    <div class="team-detail">
                      <h5 class="font-alt">HAI</h5>
                      <p class="font-serif">Saya seorang pelajar dari SMK TELKOM JAKARTA dan saya berprofesi sebagai backend developer.</p>
                      <div class="team-social"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a></div>
                    </div>
                  </div>
                  <div class="team-descr font-alt">
                    <div class="team-name">Inner Journey Tazkie Ciputra Tangguh</div>
                    <div class="team-role">Pelajar SMK TELKOM JAKARTA</div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
        <div class="module-small bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-6 col-lg-4 col-lg-offset-2">
                <div class="callout-text font-alt">
                  <h3 class="callout-title">DAFTAR MEMBER</h3>
                  <p>Klik untuk menjadi member</p>
                </div>
              </div>
              <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="callout-btn-box">
                    <div>
                        <a href="/cust" class="btn btn-g btn-round" id="subscription-form-submit">MEMBER</a></span>
                    </div>
                  <div class="text-center" id="subscription-response"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('element.credit')
        <hr class="divider-d">
        <footer class="footer bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2017&nbsp;<a href="index.html">TitaN</a>, All Rights Reserved</p>
              </div>
              <div class="col-sm-6">
                <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <!--  
    JavaScripts
    =============================================
    -->
    <script src="{{asset('titanm/assets/lib/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/wow/dist/wow.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/isotope/dist/isotope.pkgd.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/imagesloaded/imagesloaded.pkgd.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/flexslider/jquery.flexslider.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/owl.carousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/smoothscroll.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/magnific-popup/dist/jquery.magnific-popup.js')}}"></script>
    <script src="{{asset('titanm/assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js')}}"></script>
    <script src="{{asset('titanm/assets/js/plugins.js')}}"></script>
    <script src="{{asset('titanm/assets/js/main.js')}}"></script>
  </body>
</html>