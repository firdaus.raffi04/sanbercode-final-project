<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login()
    {
        return view ('login.index');
    }

    public function register()
    {
        return view ('register.index');
    }

    public function store(request $request)
    {
        $request->validate([
            'username' =>['required', 'min:3', 'max:255', 'unique:users'],
            'email' =>'required|email|unique:users',
            'password' =>'required|min:5|max:255'
        ]);

        dd('register berhasil');
    }
}
