<div class="module-small bg-dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="widget">
          <h5 class="widget-title font-alt">About Perpustakaan</h5>
          <p>The languages only differ in their grammar, their pronunciation and their most common words.</p>
          <p>Phone: +1 234 567 89 10</p>Fax: +1 234 567 89 10
          <p>Email:<a href="#">Perpustakaan@example.com</a></p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="widget">
          <h5 class="widget-title font-alt">Book Categories</h5>
          <ul class="icon-list">
            <li><a href="#">Novel</a></li>
            <li><a href="#">Kamus</a></li>
            <li><a href="#">Biografi</a></li>
            <li><a href="#">Komik</a></li>
          </ul>
        </div>
      </div>
        <div class="col-sm-4">
          <div class="widget">
            <h5 class="widget-title font-alt">Our Developer</h5>
            <ul class="widget-posts">
              <li class="clearfix">
                  <div class="widget-posts-title"><a href="#">Developer</a></div>
                  <div class="widget-posts-meta">Raffi Akbar Firdaus</div>
              </li>
              <li class="clearfix">
                  <div class="widget-posts-title"><a href="#">Front End</a></div>
                  <div class="widget-posts-meta">Inner Journey Tazkie Ciputra Tangguh</div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>