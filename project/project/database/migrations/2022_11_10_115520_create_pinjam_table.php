<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjam', function (Blueprint $table) {
            $table->id();
            $table->date('tgl_pinjam');
            $table->date('tgl_kembali');
            $table->integer('denda')->nullable();
            $table->unsignedBigInteger('id_customer');
            $table->unsignedBigInteger('id_buku');
            $table->timestamps();

            $table->foreign('id_customer')->references('id')->on('customer')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_buku')->references('id')->on('buku')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjam');
    }
}
