<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class customerController extends Controller
{
    public function home(){
        return view('customer.home');
    }

    // MEMBER
    public function member(){
        return view('customer.member');
    }
    public function daftar(Request $request){
        $request->validate([
            'nama' => 'required',
            'jk' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
        ]);
        DB::table('customer')->insert([
            "nama_lengkap" => $request["nama_lengkap"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "no_telp" => $request["no_telp"],
            "alamat" => $request["alamat"]
        ]);
        return redirect('/home');
    }
    
    // BUKU
    public function buku(){
        $buku = DB::table('buku')->get();
        return view('customer.buku', ['buku' => $buku]);
    }

    public function peminjaman($id){
        $buku = DB::table('buku')->Find($id);
        return view('customer.pinjam', ['buku' => $buku]);
    }
    public function pinjam(Request $request){
        $request->validate([
            'buku' => 'required',
            'tglpinjam' => 'required',
            'tglkembali' => 'required',
        ]);
        DB::table('pinjam')->insert([
            "id_buku" => $request["buku"],
            "tgl_pinjam" => $request["tglpinjam"],
            "tgl_kembali" => $request["tglkembali"]
        ]);
        return redirect('/buku');
    }

    public function genre($genre)
    {
        $genre = DB::table('buku')->Find($genre);
        return view('customer.buku', ['genre' => $genre]);
    }
}
