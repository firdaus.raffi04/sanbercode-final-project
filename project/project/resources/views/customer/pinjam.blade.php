@extends('layouts.cust')

@section('judul')
    Pinjam Buku
@endsection
@section('subtitle')
    Isi form berikut untuk meminjam buku di Perpustakaan,
    dan anda dapat meminjam buku apapun yang tersedia di web kami!
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <h4 class="font-alt mb-0">PINJAM BUKU</h4>
          <h6 class="font-alt mb-0">BUKU: {{$buku->judul}}</h6>
          <h6 class="font-alt mb-0">PENGARANG: {{$buku->pengarang}}</h6>
          <h6 class="font-alt mb-0">TAHUN: {{$buku->tahun}}</h6>
          <hr class="divider-w mt-10 mb-20">
          <form class="form" action="/pinjam" method="POST" role="form">
            @csrf
            <div class="form-group">
              <input class="form-control input-lg" type="hidden" name="buku" value="{{$buku->id}}" placeholder="Nama Lengkap"/>
            </div>
            <div class="form-group">
              <h6 class="font-alt mb-0">Tanggal Pinjam</h6>
              <input class="form-control" type="date" name="tglpinjam" value="Laki-laki">
            </div>
            <div class="form-group">
              <h6 class="font-alt mb-0">Tanggal Kembali</h6>
              <input class="form-control" type="date" name="tglkembali" value="Laki-laki">
            </div>
            <button class="btn btn-d btn-round" type="submit">Submit</button>
          </form>
        </div>
      </div>
@endsection