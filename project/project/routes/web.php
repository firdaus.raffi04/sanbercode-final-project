<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\customerController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Dashboard
Route::get('/', [BukuController::class, 'home']);

// Login 
Route::get('/login', [LoginController::class, 'login'] );

// Register
Route::get('/register', [LoginController::class, 'register'] );
Route::post('/register', [LoginController::class, 'store'] );


// CRUD Buku
// Create Data
Route::get('/buku/create', [BukuController::class, 'create']);
Route::post('/buku', [BukuController::class, 'store']);

//Read Data
Route::get('/buku', [BukuController::class, 'index']);
Route::get('/buku/{id}', [BukuController::class, 'show']);

//Update Data
Route::get('/buku/{id}/edit', [BukuController::class, 'edit']);
Route::put('/buku/{id}', [BukuController::class, 'update']);

// Delete Data
Route::delete('/buku/{id}', [BukuController::class,'destroy']);


//Halaman home customer
Route::get('/home', [customerController::class, 'home']);

// CRUD CUSTOMER

//Daftar Member
Route::get('/cust', [customerController::class, 'member']);
Route::post('/cust', [customerController::class, 'daftar']);

//Pinjam Buku
Route::get('/pinjam/{id}', [customerController::class, 'peminjaman']);
Route::post('/pinjam', [customerController::class, 'pinjam']);

//Halaman Buku
Route::get('/book', [customerController::class, 'buku']);
Route::get('/book/{genre}', [customerController::class, 'genre']);
Route::get('/book/{id}', [customerController::class, 'show']);

//Delete Pinjam
Route::delete('/pinjam/{id}', [customerController::class,'destroy']);