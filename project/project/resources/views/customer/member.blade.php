@extends('layouts.cust')
@section('title')
    Daftar Member
@endsection
@section('judul')
    Dengan menjadi member anda dapat meminjam buku
@endsection
@section('subtitle')
    Isi form berikut untuk menjadi member di Perpustakaan,
    dan Baca buku-buku yang tersedia di web kami!
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <h4 class="font-alt mb-0">Daftar Member</h4>
          <hr class="divider-w mt-10 mb-20">
          <form class="form" action="/cust" method="POST" role="form">
            @csrf
            <div class="form-group">
              <input class="form-control input-lg" type="text" name="nama" placeholder="Nama Lengkap"/>
            </div>
            <div class="form-group">
              <select class="form-control input-lg" type="radio" name="jk">
                <option value="Laki-laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
            <div class="form-group">
              <input class="form-control input-lg" type="text" name="alamat" placeholder="Alamat"/>
            </div>
            <div class="form-group">
              <input class="form-control input-lg" type="number" name="no_telp" placeholder="No Telp"/>
            </div>
            <button class="btn btn-d btn-round" type="submit">Submit</button>
          </form>
        </div>
      </div>
@endsection