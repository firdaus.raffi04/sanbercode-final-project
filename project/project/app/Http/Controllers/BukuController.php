<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class BukuController extends Controller
{
    public function home()
    {
        return view('element.dashboard');
    }
    
    public function index()
    {
        $buku = DB::table('buku')->get();
        return view('buku.tampil', ['buku' => $buku]);
    }

    public function create()
    {
        return view('buku.tambah');
    }

    public function store(request $request)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'gambar' => 'required',
        ]);
        
        Alert::success('Pesan Berhasil', 'Berhasil Menambahkan Data');


        DB::table('buku')->insert([
            "judul" => $request["judul"],
            "pengarang" => $request["pengarang"],
            "penerbit" => $request["penerbit"],
            "tahun" => $request["tahun"],
            "gambar" => $request["gambar"]
        ]);
        
        return redirect('/buku');
    }

    public function show($id)
    {
        $buku = DB::table('buku')->Find($id);
        return view('buku.detail', ['buku' => $buku]);
    }

    public function edit($id)
    {
        $buku = DB::table('buku')->Find($id);
        return view('buku.edit', ['buku' => $buku]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'gambar' => 'required',
        ]);

        Alert::success('Pesan Berhasil', 'Berhasil Edit Data');

        DB::table('buku')
            ->where('id', $id)
            ->update(
                [
                "judul" => $request["judul"],
                "pengarang" => $request["pengarang"],
                "penerbit" => $request["penerbit"],
                "tahun" => $request["tahun"],
                "gambar" => $request["gambar"]
                ]
                );
        return redirect('/buku');
    }

    public function destroy($id)
    {
        DB::table('buku')->where('id', '=', $id)->delete();

        Alert::success('Pesan Berhasil', 'Berhasil Hapus Data');

        return redirect('/buku');
    }

}
