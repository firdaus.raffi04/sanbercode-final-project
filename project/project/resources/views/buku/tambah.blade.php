@extends('layouts.master')
@section('title')
   Halaman Tambah Buku
@endsection
@section('subtitle')
   Tambah Buku
@endsection
@section('content')
<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Pengarang</label>
        <input type="text" class="form-control" name="pengarang" id="pengarang" placeholder="Masukkan Pengarang">
        @error('pengarang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Penerbit</label>
        <input type="text" class="form-control" name="penerbit" id="penerbit" placeholder="Masukkan Penerbit">
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" class="form-control" name="gambar" id="gambar">
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection
